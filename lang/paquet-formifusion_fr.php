<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/formidable/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'formifusion_description' => 'Conserve les traitements d\'un formulaire et fusionne ses saisies avec celles d\'un fichier Formidable',
	'formifusion_slogan' => 'Fusionner les formulaires de Formidable'
);

?>
