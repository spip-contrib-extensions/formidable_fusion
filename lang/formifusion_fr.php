<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/formidable/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//E
	'fusionner_formulaire_yaml_importer' => 'Formidable (.yaml)',

	//I
	'fusionner_formulaire' => 'Fusionner le formulaire',
	'fusionner_formulaire_fichier_label' => 'Fichier à fusionner',
	'fusionner_formulaire_format_label' => 'Format du fichier à importer',

);

?>
